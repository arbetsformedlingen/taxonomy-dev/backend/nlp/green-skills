# Green Skills

This repository contains possible relations between green skills and other concepts in Taxonomy to help Redaction Team. We used Sentence-BERT model to find relations between them.

## Set up environment

- Make sure python 3.11 is installed.
- Make sure pdm is installed.
- Install dependencies:
  `pdm install`  
  [![pdm-managed](https://img.shields.io/badge/pdm-managed-blueviolet)](https://pdm-project.org)

  ## Expected result:

  `green_skills_connected_concepts.ods` is the expected result when you run `main.py`.
