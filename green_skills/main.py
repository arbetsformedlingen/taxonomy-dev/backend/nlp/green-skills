from sentence_transformers import SentenceTransformer
from sklearn.neighbors import NearestNeighbors
import pandas as pd
import pyexcel_ods
import common
import json

model = SentenceTransformer("KBLab/sentence-bert-swedish-cased")

tax = json.loads(common.get_taxonomy_concepts())
concepts = tax["data"]["concepts"]
skill_tax_list = [x for x in concepts if x["type"] in ["skill"]]

green_skill_file = pd.read_excel("../data/Gröna_kompetenser_SBERT.xlsx")
green_skill_list = [item for sublist in green_skill_file.values for item in sublist]

# use 100
# skill_tax_list = skill_tax_list[0:100]
# green_skills_list = green_skills_list[0:100]

def get_lowercase_labels(tax_list):
    tax_labels = [common.get_preferred_labels(concept) for concept in tax_list]
    return [label.lower() for label in common.flatten(tax_labels)]


def concept_ids_dic(tax_labels, tax_list):
    dic_id = {}
    label_set = set(tax_labels)
    for concept in tax_list:
        for label in get_lowercase_labels([concept]):
            if label in label_set:
                dic_id[label] = concept["id"]
    return dic_id


def encode_vectors(labels_list):
    progress = common.ProgressReporter(len(labels_list))
    dst = []
    for label in labels_list:
        progress.end_of_iteration_message("Encoding...")
        dst.append(common.normalize(model.encode(label)))
    return dst


def nearest_neighbors(tax_labels, green_list):
    vectors = encode_vectors(tax_labels)
    neighbors = NearestNeighbors(n_neighbors=20).fit(vectors)
    distances, indices = neighbors.kneighbors(encode_vectors(green_list))
    print("distance shape is {:s}, indices shape is {:s}".format(str(distances.shape), str(indices.shape)))
    return indices


def connected_concepts_dic(green_list, tax_labels, indices, concept_ids):
    dictionary = {}
    for i, skill in enumerate(green_list):
        indices_id = {}
        indices_list = [tax_labels[j] for j in indices[i, :]]
        for index in indices_list:
            indices_id[index] = concept_ids[index]
        dictionary[skill] = indices_id
    return dictionary


def render_table(out_filename_ods, connected_concepts):
    dst = [["Classification 1 PrefLabel", "Concept Id", "Concept preferred label"]]
    for skill in connected_concepts.keys():
        for key, value in connected_concepts[skill].items():
            dst.append([skill, value, key])
    pyexcel_ods.save_data(out_filename_ods, {None: dst})


def get_connected_concepts_ods(tax_list, green_list):
    tax_labels = get_lowercase_labels(tax_list)
    concept_ids = concept_ids_dic(tax_labels, tax_list)
    indices = nearest_neighbors(tax_labels, green_list)
    connected_concepts = connected_concepts_dic(green_list, tax_labels, indices, concept_ids)
    render_table("../data/green_skills_connected_concepts.ods", connected_concepts)

def main():
    get_connected_concepts_ods(skill_tax_list, green_skill_list)

if __name__ == '__main__':
    main()
    print("connected_concepts_ods data done!")

